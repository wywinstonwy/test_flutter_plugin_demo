//
//  TestFlutterPluginView.m
//  Pods
//
//  Created by wangyun on 2022/4/15.
//

#import "TestFlutterPluginView.h"

@interface TestFlutterPluginView ()
/** channel*/
@property (nonatomic, strong)  FlutterMethodChannel  *channel;
@property (nonatomic, strong)  UIButton  *button;
@property (nonatomic, strong)  UITextField  *textField;
@property (nonatomic, strong)  UILabel  *lblText;
@property (nonatomic, assign)  NSInteger  count;
@end

@implementation TestFlutterPluginView
{
    CGRect _frame;
    int64_t _viewId;
    id _args;
   
}

- (id)initWithFrame:(CGRect)frame
  viewId:(int64_t)viewId
    args:(id)args
messager:(NSObject<FlutterBinaryMessenger>*)messenger
{
    if (self = [super init])
    {
        _frame = frame;
        _viewId = viewId;
        _args = args;
        NSLog(@"%@",args[@"titleStr"]);
        
        
        ///建立通信通道 用来 监听Flutter 的调用和 调用Fluttter 方法 这里的名称要和Flutter 端保持一致
        _channel = [FlutterMethodChannel methodChannelWithName:@"test_flutter_plugin_demo" binaryMessenger:messenger];
        
        __weak __typeof__(self) weakSelf = self;

        [_channel setMethodCallHandler:^(FlutterMethodCall * _Nonnull call, FlutterResult  _Nonnull result) {
            [weakSelf onMethodCall:call result:result];
        }];
       
    }
    return self;
}

- (UIView *)view{
    UIView *nativeView = [[UIView alloc] initWithFrame:_frame];
        nativeView.backgroundColor = [UIColor redColor];
        
    _button = [UIButton buttonWithType:UIButtonTypeSystem];
    [_button setTitle:@"我是按钮" forState:UIControlStateNormal];
    [_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_button setBackgroundColor:[UIColor blueColor]];
    _button.frame = CGRectMake(10, 20, 100, 44);
    [nativeView addSubview:_button];
    
    self.lblText = [[UILabel alloc] initWithFrame:CGRectMake(140, 20, 200, 44)];
    self.lblText.text =_args[@"titleStr"];
    self.lblText.backgroundColor =[UIColor blueColor];
    self.lblText.textColor =[UIColor whiteColor];
    [nativeView addSubview:self.lblText];
    
    [_button addTarget:self action:@selector(flutterMethod) forControlEvents:UIControlEventTouchUpInside];
    
    _textField = [[UITextField alloc] initWithFrame:CGRectMake(100, 125, 200, 44)];
    [_textField setText: [NSString stringWithFormat:@"这个数据是原生控制 %d",_count] ];
    
    
    [nativeView addSubview:_textField];
    return nativeView;
     
}
#pragma mark -- Flutter 交互监听
-(void)onMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result{
    //监听Fluter
    if ([[call method] isEqualToString:@"changeNativeTitle"]) {
        [_button setTitle:call.arguments forState:UIControlStateNormal];
    }
    
}
//调用Flutter
- (void)flutterMethod{
    self.count = self.count+1;
    NSString *str = [NSString stringWithFormat:@"原生给flutter的参数 %ld",(long)self.count];
    [self.channel invokeMethod:@"clickAciton" arguments:str];
    [_textField setText: [NSString stringWithFormat:@"这个数据是原生控制 %ld",(long)_count] ];
    if(_count%2==0){
        self.lblText.backgroundColor =[UIColor blueColor];
        self.lblText.textColor =[UIColor whiteColor];

    }else{
        self.lblText.backgroundColor =[UIColor orangeColor];
        self.lblText.textColor =[UIColor blackColor];

    }

}
@end
