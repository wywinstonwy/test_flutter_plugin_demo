//
//  TestFlutterPluginView.h
//  Pods
//
//  Created by wangyun on 2022/4/15.
//

#import <Foundation/Foundation.h>
#include <Flutter/Flutter.h>

@interface TestFlutterPluginView : NSObject<FlutterPlatformView>
- (id)initWithFrame:(CGRect)frame
             viewId:(int64_t)viewId
               args:(id)args
           messager:(NSObject<FlutterBinaryMessenger>*)messenger;
@end
