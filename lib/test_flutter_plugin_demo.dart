
import 'dart:async';

import 'package:flutter/services.dart';

typedef void TestViewCreatedCallback(TestFlutterPluginDemo controller);
class TestFlutterPluginDemo {

  late MethodChannel _channel;
  TestFlutterPluginDemo.init(int id){
    // 原生与Flutter 交互渠道
    _channel = new MethodChannel('test_flutter_plugin_demo');
    _channel.setMethodCallHandler(platformCallHandler);///设置原生参数监听
  }

  ///Flutter 调用原生
  ///这里我传了一个 字符串 当然也可以传Map
  Future<List<dynamic>?> changeNativeTitle(String str) async{
    return _channel.invokeListMethod('changeNativeTitle',str);
  }

  ///实现监听原生方法回调
  Future<dynamic> platformCallHandler(MethodCall call) async {
    switch (call.method) {
      case "clickAciton":
        print('收到原生回调 ---- $call.arguments');
        return ;
        break;
    }
  }
}



// class TestFlutterPluginDemo {
//   static const MethodChannel _channel = MethodChannel('test_flutter_plugin_demo');
//
//   static Future<String?> get platformVersion async {
//     final String? version = await _channel.invokeMethod('getPlatformVersion');
//     return version;
//   }
// }
