package com.example.test_flutter_plugin_demo;

import androidx.annotation.NonNull;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry;


/** FlutterPluginTest_1Plugin */
public class TestFlutterPluginDemoPlugin implements FlutterPlugin {

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    flutterPluginBinding.getPlatformViewRegistry().registerViewFactory("testView", new TestFlutterPluginViewFactory(flutterPluginBinding.getBinaryMessenger()));
  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
  }

  /**
   * 旧版插件加载
   *
   * @param registrar
   */
  public static void registerWith(PluginRegistry.Registrar registrar) {
    //播放器注册
    registrar.platformViewRegistry().registerViewFactory("testView", new TestFlutterPluginViewFactory(registrar.messenger()));
  }

}


