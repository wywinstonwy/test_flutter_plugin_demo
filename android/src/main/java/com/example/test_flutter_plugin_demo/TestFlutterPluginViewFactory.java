package com.example.test_flutter_plugin_demo;
import android.content.Context;

import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MessageCodec;
import io.flutter.plugin.common.StandardMessageCodec;
import io.flutter.plugin.platform.PlatformView;
import io.flutter.plugin.platform.PlatformViewFactory;


public class TestFlutterPluginViewFactory extends PlatformViewFactory {
    private BinaryMessenger messenger = null;
    public TestFlutterPluginViewFactory(BinaryMessenger messenger) {
        super(StandardMessageCodec.INSTANCE);
        this.messenger = messenger;
    }

    /**
     * @param createArgsCodec the codec used to decode the args parameter of {@link #create}.
     */
    public TestFlutterPluginViewFactory(MessageCodec<Object> createArgsCodec) {
        super(createArgsCodec);
    }

    @Override
    public PlatformView create(Context context, int viewId, Object args) {
        return new TestFlutterPluginView(context, viewId, args, this.messenger);
    }
}
