import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:test_flutter_plugin_demo/test_flutter_plugin_demo.dart';
import 'package:test_flutter_plugin_demo_example/view.dart';
void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';
  ///定义一个测试类的属性 用来调用原生方法 和原生交互
  var testFlutterPluginDemo;

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion ='是是是';

    // try {
    //   platformVersion =
    //       await TestFlutterPluginDemo.platformVersion ?? 'Unknown platform version';
    // } on PlatformException {
    //   platformVersion = 'Failed to get platform version.';
    // }


    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  @override
  Widget build(BuildContext context) {
    ///初始化 测试视图的类
    TestView testView =  TestView(
      onCreated: onTestViewCreated, key: ValueKey('testView'),
      titleStr: 'flutter给原生的参数',
    );
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Column(children: [
          Container(height: 200,width: 400,child: testView,),
          FloatingActionButton(onPressed: onNativeMethon)
        ],),
      ),
    );
  }
  void onNativeMethon(){
    this.testFlutterPluginDemo.changeNativeTitle('Flutter 调用原生成功了');
  }
  void onTestViewCreated(testFlutterPluginDemo){
    this.testFlutterPluginDemo = testFlutterPluginDemo;
  }
}
